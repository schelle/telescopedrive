/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *labelRangeSliderValue;
    QTabWidget *tabWidget;
    QWidget *tab;
    QLabel *currentPositionLabel;
    QGroupBox *HorizonGroupBox;
    QPushButton *horizonRunStop;
    QProgressBar *progressBar_2;
    QDoubleSpinBox *horizonSetSpeedDoubleSpinBox;
    QDoubleSpinBox *horizonSetPositionDoubleSpinBox;
    QDoubleSpinBox *horizonSetAccelerationDoubleSpinBox;
    QPushButton *horizonReferenceButton;
    QDoubleSpinBox *horizonSetPeriodDoubleSpinBox;
    QDoubleSpinBox *horizonSetPhaseDoubleSpinBox;
    QPushButton *horizonGonioEXE;
    QLabel *horizonReadoutValueLabel;
    QDoubleSpinBox *horizonSetSpeedRatioDoubleSpinBox;
    QLabel *speedLabel;
    QGroupBox *AzimuthGroupBox;
    QPushButton *azimuthRunStop;
    QProgressBar *progressBar;
    QDoubleSpinBox *azimuthSetSpeedDoubleSpinBox;
    QDoubleSpinBox *azimuthSetPositionDoubleSpinBox;
    QDoubleSpinBox *azimuthSetAccelerationDoubleSpinBox;
    QPushButton *azimuthReferenceButton;
    QDoubleSpinBox *azimuthSetPeriodDoubleSpinBox;
    QDoubleSpinBox *azimuthSetPhaseDoubleSpinBox;
    QLabel *azimuthReadoutValueLabel;
    QDoubleSpinBox *azimuthSetSpeedRatioDoubleSpinBox;
    QLabel *periodLabel;
    QLabel *label;
    QLabel *positionLabel;
    QLabel *gonioLabel;
    QLabel *skyMapLabel;
    QPushButton *skyTrackButton;
    QGroupBox *directControlGroupBox;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLabel *motorLabel;
    QSpinBox *motorSpinBox;
    QLabel *commandLabel;
    QSpinBox *commandSpinBox;
    QLabel *valueLabel;
    QDoubleSpinBox *valueDoubleSpinBox;
    QLabel *periodLabel_2;
    QWidget *tab_2;
    QCustomPlot *customPlot;
    QSlider *horizontalSliderGraphRange;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1215, 727);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        labelRangeSliderValue = new QLabel(centralWidget);
        labelRangeSliderValue->setObjectName(QStringLiteral("labelRangeSliderValue"));
        labelRangeSliderValue->setGeometry(QRect(450, 530, 47, 13));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 0, 1211, 661));
        tabWidget->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        currentPositionLabel = new QLabel(tab);
        currentPositionLabel->setObjectName(QStringLiteral("currentPositionLabel"));
        currentPositionLabel->setGeometry(QRect(20, 272, 47, 13));
        HorizonGroupBox = new QGroupBox(tab);
        HorizonGroupBox->setObjectName(QStringLiteral("HorizonGroupBox"));
        HorizonGroupBox->setGeometry(QRect(250, 32, 151, 301));
        horizonRunStop = new QPushButton(HorizonGroupBox);
        horizonRunStop->setObjectName(QStringLiteral("horizonRunStop"));
        horizonRunStop->setGeometry(QRect(5, 250, 141, 23));
        progressBar_2 = new QProgressBar(HorizonGroupBox);
        progressBar_2->setObjectName(QStringLiteral("progressBar_2"));
        progressBar_2->setGeometry(QRect(5, 20, 141, 16));
        progressBar_2->setValue(0);
        progressBar_2->setTextVisible(true);
        horizonSetSpeedDoubleSpinBox = new QDoubleSpinBox(HorizonGroupBox);
        horizonSetSpeedDoubleSpinBox->setObjectName(QStringLiteral("horizonSetSpeedDoubleSpinBox"));
        horizonSetSpeedDoubleSpinBox->setGeometry(QRect(5, 100, 141, 22));
        horizonSetSpeedDoubleSpinBox->setMinimum(-10000);
        horizonSetSpeedDoubleSpinBox->setMaximum(10001);
        horizonSetSpeedDoubleSpinBox->setSingleStep(0.1);
        horizonSetPositionDoubleSpinBox = new QDoubleSpinBox(HorizonGroupBox);
        horizonSetPositionDoubleSpinBox->setObjectName(QStringLiteral("horizonSetPositionDoubleSpinBox"));
        horizonSetPositionDoubleSpinBox->setGeometry(QRect(5, 70, 141, 22));
        horizonSetPositionDoubleSpinBox->setMinimum(-2.14748e+09);
        horizonSetPositionDoubleSpinBox->setMaximum(2.14748e+09);
        horizonSetAccelerationDoubleSpinBox = new QDoubleSpinBox(HorizonGroupBox);
        horizonSetAccelerationDoubleSpinBox->setObjectName(QStringLiteral("horizonSetAccelerationDoubleSpinBox"));
        horizonSetAccelerationDoubleSpinBox->setGeometry(QRect(5, 40, 141, 22));
        horizonSetAccelerationDoubleSpinBox->setMaximum(10000);
        horizonReferenceButton = new QPushButton(HorizonGroupBox);
        horizonReferenceButton->setObjectName(QStringLiteral("horizonReferenceButton"));
        horizonReferenceButton->setGeometry(QRect(5, 220, 141, 23));
        horizonSetPeriodDoubleSpinBox = new QDoubleSpinBox(HorizonGroupBox);
        horizonSetPeriodDoubleSpinBox->setObjectName(QStringLiteral("horizonSetPeriodDoubleSpinBox"));
        horizonSetPeriodDoubleSpinBox->setGeometry(QRect(5, 130, 141, 22));
        horizonSetPeriodDoubleSpinBox->setDecimals(7);
        horizonSetPeriodDoubleSpinBox->setMinimum(-10000);
        horizonSetPeriodDoubleSpinBox->setMaximum(10001);
        horizonSetPeriodDoubleSpinBox->setSingleStep(0.1);
        horizonSetPhaseDoubleSpinBox = new QDoubleSpinBox(HorizonGroupBox);
        horizonSetPhaseDoubleSpinBox->setObjectName(QStringLiteral("horizonSetPhaseDoubleSpinBox"));
        horizonSetPhaseDoubleSpinBox->setGeometry(QRect(6, 190, 102, 22));
        horizonSetPhaseDoubleSpinBox->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        horizonSetPhaseDoubleSpinBox->setDecimals(0);
        horizonSetPhaseDoubleSpinBox->setMinimum(-10000);
        horizonSetPhaseDoubleSpinBox->setMaximum(10001);
        horizonSetPhaseDoubleSpinBox->setSingleStep(0.1);
        horizonGonioEXE = new QPushButton(HorizonGroupBox);
        horizonGonioEXE->setObjectName(QStringLiteral("horizonGonioEXE"));
        horizonGonioEXE->setGeometry(QRect(114, 190, 31, 23));
        horizonReadoutValueLabel = new QLabel(HorizonGroupBox);
        horizonReadoutValueLabel->setObjectName(QStringLiteral("horizonReadoutValueLabel"));
        horizonReadoutValueLabel->setGeometry(QRect(10, 280, 131, 16));
        horizonSetSpeedRatioDoubleSpinBox = new QDoubleSpinBox(HorizonGroupBox);
        horizonSetSpeedRatioDoubleSpinBox->setObjectName(QStringLiteral("horizonSetSpeedRatioDoubleSpinBox"));
        horizonSetSpeedRatioDoubleSpinBox->setGeometry(QRect(6, 160, 141, 22));
        horizonSetSpeedRatioDoubleSpinBox->setDecimals(7);
        horizonSetSpeedRatioDoubleSpinBox->setMinimum(-10000);
        horizonSetSpeedRatioDoubleSpinBox->setMaximum(10001);
        horizonSetSpeedRatioDoubleSpinBox->setSingleStep(0.1);
        horizonRunStop->raise();
        progressBar_2->raise();
        horizonSetSpeedDoubleSpinBox->raise();
        horizonSetPositionDoubleSpinBox->raise();
        horizonSetAccelerationDoubleSpinBox->raise();
        horizonReferenceButton->raise();
        horizonSetPeriodDoubleSpinBox->raise();
        horizonSetPhaseDoubleSpinBox->raise();
        horizonGonioEXE->raise();
        horizonReadoutValueLabel->raise();
        horizonSetSpeedRatioDoubleSpinBox->raise();
        speedLabel = new QLabel(tab);
        speedLabel->setObjectName(QStringLiteral("speedLabel"));
        speedLabel->setGeometry(QRect(20, 132, 61, 20));
        AzimuthGroupBox = new QGroupBox(tab);
        AzimuthGroupBox->setObjectName(QStringLiteral("AzimuthGroupBox"));
        AzimuthGroupBox->setGeometry(QRect(90, 32, 151, 301));
        azimuthRunStop = new QPushButton(AzimuthGroupBox);
        azimuthRunStop->setObjectName(QStringLiteral("azimuthRunStop"));
        azimuthRunStop->setGeometry(QRect(5, 250, 141, 23));
        progressBar = new QProgressBar(AzimuthGroupBox);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(5, 20, 141, 16));
        progressBar->setValue(24);
        azimuthSetSpeedDoubleSpinBox = new QDoubleSpinBox(AzimuthGroupBox);
        azimuthSetSpeedDoubleSpinBox->setObjectName(QStringLiteral("azimuthSetSpeedDoubleSpinBox"));
        azimuthSetSpeedDoubleSpinBox->setGeometry(QRect(5, 100, 141, 22));
        azimuthSetSpeedDoubleSpinBox->setMinimum(-10000);
        azimuthSetSpeedDoubleSpinBox->setMaximum(10001);
        azimuthSetSpeedDoubleSpinBox->setSingleStep(0.1);
        azimuthSetPositionDoubleSpinBox = new QDoubleSpinBox(AzimuthGroupBox);
        azimuthSetPositionDoubleSpinBox->setObjectName(QStringLiteral("azimuthSetPositionDoubleSpinBox"));
        azimuthSetPositionDoubleSpinBox->setGeometry(QRect(5, 70, 141, 22));
        azimuthSetPositionDoubleSpinBox->setMinimum(-2.14748e+09);
        azimuthSetPositionDoubleSpinBox->setMaximum(2.14748e+09);
        azimuthSetAccelerationDoubleSpinBox = new QDoubleSpinBox(AzimuthGroupBox);
        azimuthSetAccelerationDoubleSpinBox->setObjectName(QStringLiteral("azimuthSetAccelerationDoubleSpinBox"));
        azimuthSetAccelerationDoubleSpinBox->setGeometry(QRect(5, 40, 141, 22));
        azimuthSetAccelerationDoubleSpinBox->setMaximum(10000);
        azimuthReferenceButton = new QPushButton(AzimuthGroupBox);
        azimuthReferenceButton->setObjectName(QStringLiteral("azimuthReferenceButton"));
        azimuthReferenceButton->setGeometry(QRect(5, 220, 141, 23));
        azimuthSetPeriodDoubleSpinBox = new QDoubleSpinBox(AzimuthGroupBox);
        azimuthSetPeriodDoubleSpinBox->setObjectName(QStringLiteral("azimuthSetPeriodDoubleSpinBox"));
        azimuthSetPeriodDoubleSpinBox->setGeometry(QRect(5, 130, 141, 22));
        azimuthSetPeriodDoubleSpinBox->setDecimals(7);
        azimuthSetPeriodDoubleSpinBox->setMinimum(-10000);
        azimuthSetPeriodDoubleSpinBox->setMaximum(10001);
        azimuthSetPeriodDoubleSpinBox->setSingleStep(0.1);
        azimuthSetPhaseDoubleSpinBox = new QDoubleSpinBox(AzimuthGroupBox);
        azimuthSetPhaseDoubleSpinBox->setObjectName(QStringLiteral("azimuthSetPhaseDoubleSpinBox"));
        azimuthSetPhaseDoubleSpinBox->setGeometry(QRect(5, 190, 141, 22));
        azimuthSetPhaseDoubleSpinBox->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        azimuthSetPhaseDoubleSpinBox->setDecimals(0);
        azimuthSetPhaseDoubleSpinBox->setMinimum(-10000);
        azimuthSetPhaseDoubleSpinBox->setMaximum(10001);
        azimuthSetPhaseDoubleSpinBox->setSingleStep(0.1);
        azimuthReadoutValueLabel = new QLabel(AzimuthGroupBox);
        azimuthReadoutValueLabel->setObjectName(QStringLiteral("azimuthReadoutValueLabel"));
        azimuthReadoutValueLabel->setGeometry(QRect(10, 280, 131, 16));
        azimuthSetSpeedRatioDoubleSpinBox = new QDoubleSpinBox(AzimuthGroupBox);
        azimuthSetSpeedRatioDoubleSpinBox->setObjectName(QStringLiteral("azimuthSetSpeedRatioDoubleSpinBox"));
        azimuthSetSpeedRatioDoubleSpinBox->setGeometry(QRect(6, 160, 141, 22));
        azimuthSetSpeedRatioDoubleSpinBox->setDecimals(7);
        azimuthSetSpeedRatioDoubleSpinBox->setMinimum(-10000);
        azimuthSetSpeedRatioDoubleSpinBox->setMaximum(10001);
        azimuthSetSpeedRatioDoubleSpinBox->setSingleStep(0.1);
        periodLabel = new QLabel(tab);
        periodLabel->setObjectName(QStringLiteral("periodLabel"));
        periodLabel->setGeometry(QRect(20, 162, 61, 20));
        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 82, 61, 16));
        positionLabel = new QLabel(tab);
        positionLabel->setObjectName(QStringLiteral("positionLabel"));
        positionLabel->setGeometry(QRect(20, 106, 61, 20));
        gonioLabel = new QLabel(tab);
        gonioLabel->setObjectName(QStringLiteral("gonioLabel"));
        gonioLabel->setGeometry(QRect(20, 230, 71, 16));
        skyMapLabel = new QLabel(tab);
        skyMapLabel->setObjectName(QStringLiteral("skyMapLabel"));
        skyMapLabel->setGeometry(QRect(420, 0, 420, 420));
        skyMapLabel->setAutoFillBackground(false);
        skyMapLabel->setScaledContents(true);
        skyTrackButton = new QPushButton(tab);
        skyTrackButton->setObjectName(QStringLiteral("skyTrackButton"));
        skyTrackButton->setGeometry(QRect(90, 352, 311, 51));
        directControlGroupBox = new QGroupBox(tab);
        directControlGroupBox->setObjectName(QStringLiteral("directControlGroupBox"));
        directControlGroupBox->setGeometry(QRect(90, 420, 311, 141));
        layoutWidget = new QWidget(directControlGroupBox);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(0, 20, 311, 101));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        motorLabel = new QLabel(layoutWidget);
        motorLabel->setObjectName(QStringLiteral("motorLabel"));

        gridLayout->addWidget(motorLabel, 0, 0, 1, 1);

        motorSpinBox = new QSpinBox(layoutWidget);
        motorSpinBox->setObjectName(QStringLiteral("motorSpinBox"));
        motorSpinBox->setMaximum(2);
        motorSpinBox->setValue(1);

        gridLayout->addWidget(motorSpinBox, 0, 1, 1, 1);

        commandLabel = new QLabel(layoutWidget);
        commandLabel->setObjectName(QStringLiteral("commandLabel"));

        gridLayout->addWidget(commandLabel, 1, 0, 1, 1);

        commandSpinBox = new QSpinBox(layoutWidget);
        commandSpinBox->setObjectName(QStringLiteral("commandSpinBox"));
        commandSpinBox->setMaximum(31);
        commandSpinBox->setValue(1);

        gridLayout->addWidget(commandSpinBox, 1, 1, 1, 1);

        valueLabel = new QLabel(layoutWidget);
        valueLabel->setObjectName(QStringLiteral("valueLabel"));

        gridLayout->addWidget(valueLabel, 2, 0, 1, 1);

        valueDoubleSpinBox = new QDoubleSpinBox(layoutWidget);
        valueDoubleSpinBox->setObjectName(QStringLiteral("valueDoubleSpinBox"));
        valueDoubleSpinBox->setMinimum(-10000);
        valueDoubleSpinBox->setMaximum(10000);
        valueDoubleSpinBox->setSingleStep(0.1);
        valueDoubleSpinBox->setValue(0);

        gridLayout->addWidget(valueDoubleSpinBox, 2, 1, 1, 1);

        periodLabel_2 = new QLabel(tab);
        periodLabel_2->setObjectName(QStringLiteral("periodLabel_2"));
        periodLabel_2->setGeometry(QRect(20, 190, 61, 20));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        customPlot = new QCustomPlot(tab_2);
        customPlot->setObjectName(QStringLiteral("customPlot"));
        customPlot->setGeometry(QRect(10, 10, 1191, 591));
        horizontalSliderGraphRange = new QSlider(tab_2);
        horizontalSliderGraphRange->setObjectName(QStringLiteral("horizontalSliderGraphRange"));
        horizontalSliderGraphRange->setGeometry(QRect(40, 610, 431, 22));
        horizontalSliderGraphRange->setMinimum(1);
        horizontalSliderGraphRange->setMaximum(2000);
        horizontalSliderGraphRange->setValue(10);
        horizontalSliderGraphRange->setOrientation(Qt::Horizontal);
        tabWidget->addTab(tab_2, QString());
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1215, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        mainToolBar->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        labelRangeSliderValue->setText(QString());
        currentPositionLabel->setText(QApplication::translate("MainWindow", "Cur. pos", 0));
        HorizonGroupBox->setTitle(QApplication::translate("MainWindow", "2-Horizon", 0));
        horizonRunStop->setText(QApplication::translate("MainWindow", "STOP", 0));
        horizonReferenceButton->setText(QApplication::translate("MainWindow", "REF", 0));
        horizonGonioEXE->setText(QApplication::translate("MainWindow", "ON", 0));
        horizonReadoutValueLabel->setText(QApplication::translate("MainWindow", "0", 0));
        speedLabel->setText(QApplication::translate("MainWindow", "Speed", 0));
        AzimuthGroupBox->setTitle(QApplication::translate("MainWindow", "1-Azimuth", 0));
        azimuthRunStop->setText(QApplication::translate("MainWindow", "STOP", 0));
        azimuthReferenceButton->setText(QApplication::translate("MainWindow", "REF", 0));
        azimuthReadoutValueLabel->setText(QApplication::translate("MainWindow", "0", 0));
        periodLabel->setText(QApplication::translate("MainWindow", "Period", 0));
        label->setText(QApplication::translate("MainWindow", "Acceleration", 0));
        positionLabel->setText(QApplication::translate("MainWindow", "Position", 0));
        gonioLabel->setText(QApplication::translate("MainWindow", "Phase - Gonio", 0));
        skyMapLabel->setText(QString());
        skyTrackButton->setText(QApplication::translate("MainWindow", "SkyTrack", 0));
        directControlGroupBox->setTitle(QApplication::translate("MainWindow", "Direct Control", 0));
        motorLabel->setText(QApplication::translate("MainWindow", "Motor", 0));
        commandLabel->setText(QApplication::translate("MainWindow", "Command", 0));
        valueLabel->setText(QApplication::translate("MainWindow", "Value", 0));
        periodLabel_2->setText(QApplication::translate("MainWindow", "Speed Ratio", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Control", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Graph", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
