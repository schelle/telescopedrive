/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../TelescopeDriveControl_GUI/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[43];
    char stringdata0[1075];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 5), // "graph"
QT_MOC_LITERAL(2, 17, 0), // ""
QT_MOC_LITERAL(3, 18, 3), // "azi"
QT_MOC_LITERAL(4, 22, 3), // "hor"
QT_MOC_LITERAL(5, 26, 11), // "sendMessage"
QT_MOC_LITERAL(6, 38, 7), // "command"
QT_MOC_LITERAL(7, 46, 17), // "buildDriveCommand"
QT_MOC_LITERAL(8, 64, 14), // "serialReceived"
QT_MOC_LITERAL(9, 79, 33), // "on_commandSpinBox_editingFini..."
QT_MOC_LITERAL(10, 113, 31), // "on_motorSpinBox_editingFinished"
QT_MOC_LITERAL(11, 145, 25), // "on_azimuthRunStop_clicked"
QT_MOC_LITERAL(12, 171, 11), // "sendCommand"
QT_MOC_LITERAL(13, 183, 5), // "motor"
QT_MOC_LITERAL(14, 189, 5), // "value"
QT_MOC_LITERAL(15, 195, 25), // "on_horizonRunStop_clicked"
QT_MOC_LITERAL(16, 221, 37), // "on_valueDoubleSpinBox_editing..."
QT_MOC_LITERAL(17, 259, 33), // "on_azimuthReferenceButton_cli..."
QT_MOC_LITERAL(18, 293, 33), // "on_horizonReferenceButton_cli..."
QT_MOC_LITERAL(19, 327, 41), // "on_horizontalSliderGraphRange..."
QT_MOC_LITERAL(20, 369, 8), // "position"
QT_MOC_LITERAL(21, 378, 26), // "on_horizonGonioEXE_clicked"
QT_MOC_LITERAL(22, 405, 51), // "on_azimuthSetAccelerationDoub..."
QT_MOC_LITERAL(23, 457, 19), // "azimuthAcceleration"
QT_MOC_LITERAL(24, 477, 47), // "on_azimuthSetPositionDoubleSp..."
QT_MOC_LITERAL(25, 525, 15), // "azimuthPosition"
QT_MOC_LITERAL(26, 541, 44), // "on_azimuthSetSpeedDoubleSpinB..."
QT_MOC_LITERAL(27, 586, 12), // "azimuthSpeed"
QT_MOC_LITERAL(28, 599, 45), // "on_azimuthSetPeriodDoubleSpin..."
QT_MOC_LITERAL(29, 645, 13), // "azimuthPeriod"
QT_MOC_LITERAL(30, 659, 51), // "on_horizonSetAccelerationDoub..."
QT_MOC_LITERAL(31, 711, 19), // "horizonAcceleration"
QT_MOC_LITERAL(32, 731, 47), // "on_horizonSetPositionDoubleSp..."
QT_MOC_LITERAL(33, 779, 15), // "horizonPosition"
QT_MOC_LITERAL(34, 795, 44), // "on_horizonSetSpeedDoubleSpinB..."
QT_MOC_LITERAL(35, 840, 12), // "horizonSpeed"
QT_MOC_LITERAL(36, 853, 45), // "on_horizonSetPeriodDoubleSpin..."
QT_MOC_LITERAL(37, 899, 13), // "horizonPeriod"
QT_MOC_LITERAL(38, 913, 25), // "on_skyTrackButton_clicked"
QT_MOC_LITERAL(39, 939, 49), // "on_azimuthSetSpeedRatioDouble..."
QT_MOC_LITERAL(40, 989, 17), // "azimuthSpeedRatio"
QT_MOC_LITERAL(41, 1007, 49), // "on_horizonSetSpeedRatioDouble..."
QT_MOC_LITERAL(42, 1057, 17) // "horizonSpeedRatio"

    },
    "MainWindow\0graph\0\0azi\0hor\0sendMessage\0"
    "command\0buildDriveCommand\0serialReceived\0"
    "on_commandSpinBox_editingFinished\0"
    "on_motorSpinBox_editingFinished\0"
    "on_azimuthRunStop_clicked\0sendCommand\0"
    "motor\0value\0on_horizonRunStop_clicked\0"
    "on_valueDoubleSpinBox_editingFinished\0"
    "on_azimuthReferenceButton_clicked\0"
    "on_horizonReferenceButton_clicked\0"
    "on_horizontalSliderGraphRange_sliderMoved\0"
    "position\0on_horizonGonioEXE_clicked\0"
    "on_azimuthSetAccelerationDoubleSpinBox_valueChanged\0"
    "azimuthAcceleration\0"
    "on_azimuthSetPositionDoubleSpinBox_valueChanged\0"
    "azimuthPosition\0"
    "on_azimuthSetSpeedDoubleSpinBox_valueChanged\0"
    "azimuthSpeed\0on_azimuthSetPeriodDoubleSpinBox_valueChanged\0"
    "azimuthPeriod\0"
    "on_horizonSetAccelerationDoubleSpinBox_valueChanged\0"
    "horizonAcceleration\0"
    "on_horizonSetPositionDoubleSpinBox_valueChanged\0"
    "horizonPosition\0"
    "on_horizonSetSpeedDoubleSpinBox_valueChanged\0"
    "horizonSpeed\0on_horizonSetPeriodDoubleSpinBox_valueChanged\0"
    "horizonPeriod\0on_skyTrackButton_clicked\0"
    "on_azimuthSetSpeedRatioDoubleSpinBox_valueChanged\0"
    "azimuthSpeedRatio\0"
    "on_horizonSetSpeedRatioDoubleSpinBox_valueChanged\0"
    "horizonSpeedRatio"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,  139,    2, 0x08 /* Private */,
       5,    1,  144,    2, 0x08 /* Private */,
       7,    0,  147,    2, 0x08 /* Private */,
       8,    0,  148,    2, 0x08 /* Private */,
       9,    0,  149,    2, 0x08 /* Private */,
      10,    0,  150,    2, 0x08 /* Private */,
      11,    0,  151,    2, 0x08 /* Private */,
      12,    3,  152,    2, 0x08 /* Private */,
      15,    0,  159,    2, 0x08 /* Private */,
      16,    0,  160,    2, 0x08 /* Private */,
      17,    0,  161,    2, 0x08 /* Private */,
      18,    0,  162,    2, 0x08 /* Private */,
      19,    1,  163,    2, 0x08 /* Private */,
      21,    0,  166,    2, 0x08 /* Private */,
      22,    1,  167,    2, 0x08 /* Private */,
      24,    1,  170,    2, 0x08 /* Private */,
      26,    1,  173,    2, 0x08 /* Private */,
      28,    1,  176,    2, 0x08 /* Private */,
      30,    1,  179,    2, 0x08 /* Private */,
      32,    1,  182,    2, 0x08 /* Private */,
      34,    1,  185,    2, 0x08 /* Private */,
      36,    1,  188,    2, 0x08 /* Private */,
      38,    0,  191,    2, 0x08 /* Private */,
      39,    1,  192,    2, 0x08 /* Private */,
      41,    1,  195,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Long, QMetaType::Long,    3,    4,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::Double,   13,    6,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   23,
    QMetaType::Void, QMetaType::Double,   25,
    QMetaType::Void, QMetaType::Double,   27,
    QMetaType::Void, QMetaType::Double,   29,
    QMetaType::Void, QMetaType::Double,   31,
    QMetaType::Void, QMetaType::Double,   33,
    QMetaType::Void, QMetaType::Double,   35,
    QMetaType::Void, QMetaType::Double,   37,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   40,
    QMetaType::Void, QMetaType::Double,   42,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->graph((*reinterpret_cast< long(*)>(_a[1])),(*reinterpret_cast< long(*)>(_a[2]))); break;
        case 1: _t->sendMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->buildDriveCommand(); break;
        case 3: _t->serialReceived(); break;
        case 4: _t->on_commandSpinBox_editingFinished(); break;
        case 5: _t->on_motorSpinBox_editingFinished(); break;
        case 6: _t->on_azimuthRunStop_clicked(); break;
        case 7: _t->sendCommand((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 8: _t->on_horizonRunStop_clicked(); break;
        case 9: _t->on_valueDoubleSpinBox_editingFinished(); break;
        case 10: _t->on_azimuthReferenceButton_clicked(); break;
        case 11: _t->on_horizonReferenceButton_clicked(); break;
        case 12: _t->on_horizontalSliderGraphRange_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_horizonGonioEXE_clicked(); break;
        case 14: _t->on_azimuthSetAccelerationDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->on_azimuthSetPositionDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 16: _t->on_azimuthSetSpeedDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 17: _t->on_azimuthSetPeriodDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->on_horizonSetAccelerationDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 19: _t->on_horizonSetPositionDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 20: _t->on_horizonSetSpeedDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 21: _t->on_horizonSetPeriodDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 22: _t->on_skyTrackButton_clicked(); break;
        case 23: _t->on_azimuthSetSpeedRatioDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 24: _t->on_horizonSetSpeedRatioDoubleSpinBox_valueChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
