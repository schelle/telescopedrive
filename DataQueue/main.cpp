
/** main.cpp demonstrator
* Main function only demonstrates DataQueue functionality
* 
*
* \file	main.cpp
* \author	Igor Schelle igor.schelle@tescan.com
* \date	2017/3
*/

#include "tsc_data_queue.hpp"
#include <vector>
#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <functional>
#include <memory>
#include <mutex>
#include <chrono>
#include <thread>

using namespace Tescan;

std::mutex mu;
const std::uint16_t height = 2160; 
const std::uint16_t width = 2560;
const std::uint16_t cycles = 1000; // number of frames

std::vector<std::uint16_t> vec(height *width); // dummy data of approximately memory size as Zyla image

std::shared_ptr< Tescan::DataQueue<std::vector<std::uint16_t>>> cache = std::make_shared<Tescan::DataQueue<std::vector<std::uint16_t>>>(); //Cache pointer

void writeCache() // f-ction which pushing @ "high speed" data to cache
{
	for (int i = 0; i < cycles; i++){
		cache->push(vec);
		{
		std::lock_guard<std::mutex> lock(mu);
		std::cout << std::endl;
		std::cout << "WRITE Items in Memory: " << cache->memoryItemsSize() << std::endl;
		std::cout << "WRITE Items on disk: " << cache->diskItemsSize() << std::endl;
		std::cout << "WRITE Items total: " << cache->totalSize() << std::endl;
		}
	}
}

void cacheRead() // reading function is by purpose delayed to simulate Ethernet throughput 10 FPS
{
	for (int i = 0; i < cycles; i++){
		if (cache->empty()){
			i--;
			std::cout << "Cache is empty" << std::endl;
		}
		else {
			std::vector<std::uint16_t> retVec =  cache->front();
			cache->pop();{
				std::lock_guard<std::mutex> lock(mu);
				std::cout << std::endl;
				std::cout << "READ Items in Memory: " << cache->memoryItemsSize() << std::endl;
				std::cout << "READ Items on disk: " << cache->diskItemsSize() << std::endl;
				std::cout << "READ Items total: " << cache->totalSize() << std::endl;
			}
		}
		Sleep(100); // in miliseconds
	}
}

int main()
{
	// delete all previously saved and non-picked data
	cache->clear();

	// set some data to vector
	for(auto item : vec) {
	    item = 65535;
	}

	cache->setDumpDirectory("C:/testDump");
	cache->setDiskLimit(90.0);
	cache->setMemoryLimit(90.0);

	std::thread t1(writeCache);

	std::thread t2(cacheRead);

	if (t1.joinable()){
		t1.join();
	}
	
	if(t2.joinable()){
		t2.join();
	}
	
    return 0;
}