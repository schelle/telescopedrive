/** Data Queue template class implementation
*
* Container wrapper cache template class can work with any type. Custom type has to implement
* Boost serialization friend functions to be able dump themselves to disk.
* This cache works in two modes Memory Queue(RAM) and Disk Queue. When Memory queue is full
* (reach some threshold) starts dumping objects to disc. This operation is expensive.
*
*
* TODO: error return values to specific enum
*
* \file	tsc_data_queue.hpp
* \author	Igor Schelle igor.schelle@tescan.com
* \date	2017/3
*/

//                                                       +  HDD dump threshold
//                               Memory queue            ^                  Switch   +--------+
//                   +--------------------------------------+--+--+--+--+            |   CAM  |
//            +------+XX|XX|XX|XX|XX|XX|XX|XX|XX|XX|XX|XX|  |  |  |  |  | <--   +----+   DET. |
//            |      +--------------------------------------+--+--+--+--+       |    |        |
//            |                                          ^                      |    +--------+
//            |                                          +                      |
//            |                  Address queue                                  |
//            |      +--------+--+--+--+--+--+--+--+--+--+--+--+--+--+--+       |
//  Output    |      |XX|XX|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |       |
//<-----------+      ++--+----+--+--+--+--+--+--+--+--+--+--+--+--+--+--+       |
//                    |  |                                                      |
//                    |  |                                     +                |
//            +       |  |       Binary file HDD queue         ^                |
//            |      +v--v----+--+--+--+--+--+--+--+--+--+--+-----+--+--+       |
//            +------+XX|XX|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | <-----+  Input
//                   +--------+--+--+--+--+--+--+--+--+--+--+-----+--+--+
//                                                             ^
//                                                             +  Acq. stop threshold

#ifndef DATA_QUEUE_HPP
#define DATA_QUEUE_HPP
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/filesystem.hpp>
#include "Windows.h"
#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include <mutex>
#include "tsc_pub_sub.hpp"


namespace Tescan {

struct TscDataQueueObservers {
    enum {
        DiskFull,
        Restart
    };
    
    using ObserverTable = std::tuple
                          <
                          Observer<void()>,
                          Observer<void()>
                          >;
};


using namespace std::experimental::filesystem;

template<typename T> class DataQueue: public Observable<TscDataQueueObservers> {
public:
    DataQueue() = default;
    
    ~DataQueue() = default;

	DataQueue(DataQueue<T> &that) = delete;

	DataQueue<T> operator = (DataQueue<T> &that) = delete;
        
    /** inserts element at the end
    * \param[in] item		any object to be pushed
    */
	std::int8_t push(T &item);
    
    /// clears the contents (including non-picked files )
    std::int16_t clear();
    
    /**  changes destination of stored object
    * \param[in] enableDiskStorage		when true. data are dumped to the dick
    */
    void diskSwitch(bool enableDiskStorage);
    
    /**  specifies destination for dumped files
    * \param[in] dumpDir	desired directory (must exist)
    */
    void setDumpDirectory(const path dumpDir);
    
    /** specifies limit for maximum memory (HDD) usage  (0 - 100%)
    *	when queue reaches this limit data are to be stored on disk
    * \param[in]
    */
    void setMemoryLimit(const std::float_t limit);
    
    /** specifies limit for maximum disk usage (0 - 100%)
    *	when queue reaches this limit. acquisition pause callback is triggered
    * \param[in]
    */
    void setDiskLimit(const std::double_t limit);
    
    /// checks whether the container is empty
    bool empty() const;
        
    /// returns amount of objects stored in memory
    std::size_t memoryItemsSize() const;
    
    /// returns amount of objects stored on disk
    std::size_t diskItemsSize() const;
    
    /// returns total amount of object stored at queue
    std::size_t totalSize() const;
    
	/**access the first element
	*IPORTANT NOTE: Before use front() check i queue is empty
	*/
	T front();
    
	/**access the last element
	*IPORTANT NOTE: Before use back() check i queue is empty
	*/
	T back();

	/**removes the first element
	*IPORTANT NOTE: Before use pop() check i queue is empty
	*/
	std::int8_t pop();
    
private:

    std::int16_t save(const T &item, path dumpPath);

    std::int16_t load(T &item, path dumpPath);

	/// returns memory (HDD) usage
	std::float_t memoryUsage() const;

	/// returns disk usage
	std::double_t diskUsage() const;
    
    std::deque<T>							m_cache;
    std::deque<path>						m_dumpedFiles;
    path									m_dumpDir = "C:/testDump";
    path									m_dumpPath;
    std::size_t								m_elementCounter = 0;
    std::size_t								m_maxSize = 0;
    bool									m_enableDiskStorege = false;
    bool									m_paused = false;
    std::double_t							m_diskPercentageLimit = 80.0;
    std::float_t							m_memoryPercentageLimit = 55.0;
    std::mutex								m_deque_lock;
    
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::float_t DataQueue<T>::memoryUsage() const
{
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    return static_cast<float>(statex.dwMemoryLoad);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::double_t DataQueue<T>::diskUsage() const
{
    std::string foo = m_dumpDir.root_path().string();
    space_info si = space(foo);
    return 100.0 - ((static_cast<std::double_t>(si.available) / static_cast<std::double_t>(si.capacity)) *100.0);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::size_t DataQueue<T>::memoryItemsSize() const
{
    return m_cache.size();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::size_t DataQueue<T>::diskItemsSize() const
{
    return m_dumpedFiles.size();
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::int16_t DataQueue<T>::save(const T &item, path dumpPath)
{
    if(!boost::filesystem::exists(m_dumpDir.string())) {
        boost::system::error_code errcode;
        boost::filesystem::create_directory(m_dumpDir.string(), errcode);
        if(errcode) {
            return -3;
        }
    }
    std::ofstream ofs(dumpPath.string(), std::ios::binary);
    if(ofs) {
        boost::archive::binary_oarchive oa(ofs);
        oa << item;
        return 0;
    }
    else {
        return -1;
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::int16_t DataQueue<T>::load(T &item, path dumpPath)
{
    if(!boost::filesystem::exists(m_dumpDir.string())) {
        return -3;
    }
    std::ifstream ifs(dumpPath.string(), std::ios::binary);
    if(ifs) {
        boost::archive::binary_iarchive ia(ifs);
        ia >> item;
        return 0;
    }
    else {
        return -2;
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::int8_t DataQueue<T>::pop()
{
	int err = 0;
    if(memoryItemsSize() != 0) {
        std::lock_guard<std::mutex> lock(m_deque_lock);
        m_cache.pop_front();
		m_elementCounter--;
    }
    else {
        std::string deleteFile;
        {
            std::lock_guard<std::mutex> lock(m_deque_lock);
            deleteFile = m_dumpedFiles.front().string();
            m_dumpedFiles.pop_front();
			m_elementCounter--;
        }
		err = std::remove(deleteFile.c_str());
    }
   
    // restart when drained after being full
    if((m_paused == true) && (totalSize() == 0)) {
        notifyObserver<TscDataQueueObservers::Restart>();
    }
	return err;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline void DataQueue<T>::diskSwitch(bool enableDiskStorage)
{
	std::lock_guard<std::mutex> lock(m_deque_lock);
    m_enableDiskStorege = enableDiskStorage;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::int8_t DataQueue<T>::push(T &item)
{
	std::lock_guard<std::mutex> lock(m_deque_lock);
	if ( m_enableDiskStorege || (m_memoryPercentageLimit < memoryUsage()) ) {
		if (m_diskPercentageLimit < diskUsage()) {
			notifyObserver<TscDataQueueObservers::DiskFull>();
			m_paused = true;
			return 1;
		}
		std::string fileName = "stream" + std::to_string(m_elementCounter) + ".cck";
		m_dumpPath = m_dumpDir / fileName;

		if (!save(item, m_dumpPath)) {
			m_dumpedFiles.push_back(m_dumpPath);
		}
	}
    else {
		m_cache.push_back(item);
    }
	m_elementCounter++;
	return 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline T DataQueue<T>::front()
{
    T item;
    
    std::uint16_t usage = static_cast<std::uint16_t>(memoryUsage());
    
    if(memoryItemsSize() != 0) {
        std::lock_guard<std::mutex> lock(m_deque_lock);
        item = m_cache.front();
    }
    else {
        path archivePath;
        {
            std::lock_guard<std::mutex> lock(m_deque_lock);
            archivePath = m_dumpedFiles.front();
        }
        load(item, archivePath);
    }
    return item;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline T DataQueue<T>::back()
{
    T item;
    
    std::uint16_t usage = static_cast<std::uint16_t>(memoryUsage());
    
    if(memoryItemsSize() != 0) {
        std::lock_guard<std::mutex> lock(m_deque_lock);
        item = m_cache.back();
        
    }
    else {
        path archivePath;
        {
            std::lock_guard<std::mutex> lock(m_deque_lock);
            archivePath = m_dumpedFiles.back();
        }
        load(item, archivePath);
    }
    
    return item;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::size_t DataQueue<T>::totalSize() const
{
    return m_elementCounter;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline void DataQueue<T>::setMemoryLimit(const std::float_t limit)
{
    if(limit > 99.0f) {
        m_memoryPercentageLimit = 99.0f;
    }
    else {
        m_memoryPercentageLimit = limit;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T>inline void DataQueue<T>::setDiskLimit(const std::double_t limit)
{
    if(limit > 99.0) {
        m_diskPercentageLimit = 99;
    }
    else {
        m_diskPercentageLimit = limit;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline void DataQueue<T>::setDumpDirectory(const path dumpDir)
{
    m_dumpDir = dumpDir;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline bool DataQueue<T>::empty() const
{
    if(m_cache.empty() && m_dumpedFiles.empty()) {
        return true;
    }
    else {
        return false;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
template<typename T> inline std::int16_t DataQueue<T>::clear()
{
    if(!m_cache.empty()) {
        m_cache.clear();
    }
    if(!m_dumpedFiles.empty()) {
    
        m_dumpedFiles.clear();
    }
    boost::system::error_code errcode;
    boost::filesystem::remove_all(m_dumpDir.string(),errcode);
    if(errcode) {
        return -1;
    }
    return 0;
}

#endif // !DATA_QUEUE_HPP


}