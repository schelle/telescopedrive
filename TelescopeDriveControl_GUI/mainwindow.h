#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void graph(long azi, long hor);

    void sendMessage(QString command);

    void buildDriveCommand(void);

    void serialReceived();

    void on_commandSpinBox_editingFinished();

    void on_motorSpinBox_editingFinished();

    void on_azimuthRunStop_clicked();

    void sendCommand(int motor, int command, double value);

    void on_horizonRunStop_clicked();

    void on_valueDoubleSpinBox_editingFinished();

    void on_azimuthReferenceButton_clicked();

    void on_horizonReferenceButton_clicked();

    void on_horizontalSliderGraphRange_sliderMoved(int position);

    void on_horizonGonioEXE_clicked();

    void on_azimuthSetAccelerationDoubleSpinBox_valueChanged(double azimuthAcceleration);

    void on_azimuthSetPositionDoubleSpinBox_valueChanged(double azimuthPosition);

    void on_azimuthSetSpeedDoubleSpinBox_valueChanged(double azimuthSpeed);

    void on_azimuthSetPeriodDoubleSpinBox_valueChanged(double azimuthPeriod);

    void on_horizonSetAccelerationDoubleSpinBox_valueChanged(double horizonAcceleration);

    void on_horizonSetPositionDoubleSpinBox_valueChanged(double horizonPosition);

    void on_horizonSetSpeedDoubleSpinBox_valueChanged(double horizonSpeed);

    void on_horizonSetPeriodDoubleSpinBox_valueChanged(double horizonPeriod);

    void on_skyTrackButton_clicked();

    void on_azimuthSetSpeedRatioDoubleSpinBox_valueChanged(double azimuthSpeedRatio);

    void on_horizonSetSpeedRatioDoubleSpinBox_valueChanged(double horizonSpeedRatio);

private:
    Ui::MainWindow *ui;
    QSerialPort *arduino;
    QString arduinoPortName;
    bool arduinoIsAvailable;
    static const quint16 arduinoUnoVendoID = 10755; //9026 DUE 10755
    static const quint16 arduinoUnoProductID = 61; //1 - 67 DUE 61

    int m_motor;
    int m_command;
    char m_outputRegAddress;

    QByteArray serialData;
    QString serialBuffer;
    QString parsed_data;

    QString azimuthReadoutValue;
    QString horizonReadoutValue;
    long azimuth;
    long horizon;
    int graphRange;

};

#endif // MAINWINDOW_H
