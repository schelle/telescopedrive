#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QtMath>
#include <QtWidgets> //dialogová okna

//--------------------------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    arduinoIsAvailable = false;
    arduinoPortName = "";

    m_outputRegAddress = 0;
    m_motor = 0;
    m_command = 0;

    //SETUP PLOT
    ui->customPlot->addGraph(); // blue line
    ui->customPlot->graph(0)->setPen(QPen(Qt::blue));
    ui->customPlot->addGraph(); // red line
    ui->customPlot->graph(1)->setPen(QPen(Qt::red));
    ui->customPlot->addGraph(); // blue dot
    ui->customPlot->graph(2)->setPen(QPen(Qt::blue));
    ui->customPlot->graph(2)->setLineStyle(QCPGraph::lsNone);
    ui->customPlot->graph(2)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->customPlot->addGraph(); // red dot
    ui->customPlot->graph(3)->setPen(QPen(Qt::red));
    ui->customPlot->graph(3)->setLineStyle(QCPGraph::lsNone);
    ui->customPlot->graph(3)->setScatterStyle(QCPScatterStyle::ssDisc);
    ui->customPlot->addGraph(); // green line
    ui->customPlot->graph(4)->setPen(QPen(Qt::green));

    ui->customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    ui->customPlot->xAxis->antialiased();
    ui->customPlot->xAxis->setDateTimeFormat("ss");
    ui->customPlot->xAxis->setAutoTickStep(true);
    ui->customPlot->axisRect()->setupFullAxesBox();

    // make left and bottom axes transfer their ranges to right and top axes:
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    graphRange = 10;

    QPixmap skyMapPixMap(":/pic/mapa.png");

    ui->skyMapLabel->setPixmap(skyMapPixMap);

    arduino = new QSerialPort;

    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts() ){
        if(serialPortInfo.hasVendorIdentifier() && serialPortInfo.hasProductIdentifier()){
            if(serialPortInfo.vendorIdentifier() == arduinoUnoVendoID){
                if(serialPortInfo.productIdentifier() == arduinoUnoProductID){
                    arduinoPortName = serialPortInfo.portName();
                    arduinoIsAvailable = true;

                }
            }
        }
    }
    if(arduinoIsAvailable){
        arduino->setPortName(arduinoPortName);
        arduino->open(QSerialPort::ReadWrite);
        arduino->setBaudRate(QSerialPort::Baud9600);
        arduino->setDataBits(QSerialPort::Data8);
        arduino->setParity(QSerialPort::NoParity);
        arduino->setStopBits(QSerialPort::OneStop);
        arduino->setFlowControl(QSerialPort::NoFlowControl);
        connect(arduino,SIGNAL(readyRead()),this,SLOT(serialReceived()));
    }else{
        QMessageBox::warning(this, "Port error", "Couldn't find Arduino!");
    }

    ui->azimuthSetAccelerationDoubleSpinBox->setKeyboardTracking(false);
    ui->azimuthSetPeriodDoubleSpinBox->setKeyboardTracking(false);
    ui->azimuthSetPositionDoubleSpinBox->setKeyboardTracking(false);
    ui->azimuthSetSpeedDoubleSpinBox->setKeyboardTracking(false);

    ui->horizonSetAccelerationDoubleSpinBox->setKeyboardTracking(false);
    ui->horizonSetPositionDoubleSpinBox->setKeyboardTracking(false);
    ui->horizonSetPeriodDoubleSpinBox->setKeyboardTracking(false);
    ui->horizonSetSpeedDoubleSpinBox->setKeyboardTracking(false);
}

//--------------------------------------------------------------------------------------------
MainWindow::~MainWindow()
{
    if(arduino->isOpen()){
        arduino->close();
    }
    delete ui;
}

//--------------------------------------------------------------------------------------------
void MainWindow::buildDriveCommand(void){
    m_outputRegAddress = (m_motor << 5) + m_command;
    qDebug() <<  "m_outputRegAddress" << m_outputRegAddress - '0';
}

//--------------------------------------------------------------------------------------------
void MainWindow::sendCommand(int motor, int command, double value)
{
    char regAddress = (motor << 5) + command;
    QString message =  QString("%1%2").arg(regAddress).arg(value);
    if(arduino->isWritable()){
        arduino->write(message.toStdString().c_str());
        qDebug() << message;
    }else{
        qDebug()<< "Couldn't write serial!";
    }
}

//--------------------------------------------------------------------------------------------
void MainWindow::sendMessage(QString message){
    if(arduino->isWritable()){
        arduino->write(message.toStdString().c_str());
        qDebug() << message;
    }else{
        qDebug()<< "Couldn't write serial!";
    }
}

//--------------------------------------------------------------------------------------------
void MainWindow::serialReceived()
    {
        QStringList buffer_split = serialBuffer.split('\n',QString::KeepEmptyParts);
        if(buffer_split.length() < 3){
            // no parsed value yet so continue accumulating bytes from serial in the buffer.
            serialData = arduino->readAll();
            serialBuffer = serialBuffer + QString::fromStdString(serialData.toStdString());
            serialData.clear();
        }else{
            // the second element of buffer_split is parsed correctly, update the temperature value on temp_lcdNumber
            serialBuffer = "";
            qDebug() << "Incoming data: " << buffer_split << "\n";
            parsed_data = buffer_split[0];
            QStringList valueBufferSplit = parsed_data.split(',',QString::KeepEmptyParts);
            qDebug() << "ValueBuferSplit: " << valueBufferSplit<< "\n";
            if(valueBufferSplit.length() == 2){
                horizonReadoutValue = valueBufferSplit[1];
                azimuthReadoutValue  = valueBufferSplit[0];
                ui->azimuthReadoutValueLabel->setText(azimuthReadoutValue);
                ui->horizonReadoutValueLabel->setText(horizonReadoutValue);;
            }
        }
        bool okAz = false;
        bool okHo = false;
        azimuth = azimuthReadoutValue.toLong(&okAz,10);
        horizon = horizonReadoutValue.toLong(&okHo,10);
        if(okAz&&okHo){
            emit(graph(azimuth,horizon));
        }
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_valueDoubleSpinBox_editingFinished()
{
    float cmd = ui->valueDoubleSpinBox->value();
    MainWindow::sendMessage(QString("%1%2").arg(m_outputRegAddress).arg(cmd));
    qDebug() << cmd;
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_commandSpinBox_editingFinished()
{
    m_command = ui->commandSpinBox->value();
    qDebug() << "m_command: " << m_command;
    emit MainWindow::buildDriveCommand();
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_motorSpinBox_editingFinished()
{
    m_motor = ui->motorSpinBox->value();
    qDebug() << "m_motor: " << m_motor;
    emit MainWindow::buildDriveCommand();
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthRunStop_clicked()
{
    MainWindow::sendCommand(1,1,0);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonRunStop_clicked()
{
    MainWindow::sendCommand(2,1,0);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthReferenceButton_clicked()
{
    MainWindow::sendCommand(1,4,0);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonReferenceButton_clicked()
{
    MainWindow::sendCommand(2,4,0);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonGonioEXE_clicked()
{
    if(ui->horizonGonioEXE->text() == "ON")
    {
      qDebug() << "ON";
      ui->horizonGonioEXE->setText("OFF");
      MainWindow::sendCommand(2,5,ui->horizonSetPhaseDoubleSpinBox->value());
      MainWindow::sendCommand(1,5,ui->azimuthSetPhaseDoubleSpinBox->value());
    }
    else
    {
      qDebug() << "OFF";
      ui->horizonGonioEXE->setText("ON");
      MainWindow::sendCommand(2,6,0);
      MainWindow::sendCommand(1,6,0);
    }
}

//--------------------------------------------------------------------------------------------
void MainWindow::graph(long azi, long hor)
{
    // calculate two new data points:
    double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
    static double lastPointKey = 0;
    if (key-lastPointKey > 0.09) // at most add point every 10 ms
    {

      // add data to lines:
      ui->customPlot->graph(0)->addData(key, azi);
      ui->customPlot->graph(1)->addData(key, hor);
      ui->customPlot->graph(4)->addData(key, sqrt( (azi*azi)+(hor*hor) ));
      // set data of dots:
      ui->customPlot->graph(2)->clearData();
      ui->customPlot->graph(2)->addData(key, azi);
      ui->customPlot->graph(3)->clearData();
      ui->customPlot->graph(3)->addData(key, hor);

      // remove data of lines that's outside visible range:
      ui->customPlot->graph(0)->removeDataBefore(key-graphRange);
      ui->customPlot->graph(1)->removeDataBefore(key-graphRange);
      ui->customPlot->graph(4)->removeDataBefore(key-graphRange);
      // rescale value (vertical) axis to fit the current data:
      ui->customPlot->graph(0)->rescaleValueAxis();
      ui->customPlot->graph(1)->rescaleValueAxis(true);
      ui->customPlot->graph(4)->rescaleValueAxis(true);
      lastPointKey = key;
    }
    // make key axis range scroll with the data (at a constant range size of 8):
    ui->customPlot->xAxis->setRange(key+0.25, graphRange, Qt::AlignRight);
    ui->customPlot->replot();

    // calculate frames per second:
    static double lastFpsKey;
    static int frameCount;
    ++frameCount;
    if (key-lastFpsKey > 2) // average fps over 2 seconds
    {
      ui->statusBar->showMessage(
            QString("%1 FPS, Total Data points: %2")
            .arg(frameCount/(key-lastFpsKey), 0, 'f', 0)
            .arg(ui->customPlot->graph(0)->data()->count()+ui->customPlot->graph(1)->data()->count())
            , 0);
      lastFpsKey = key;
      frameCount = 0;
    }
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizontalSliderGraphRange_sliderMoved(int position)
{
    graphRange = position;
    QString range;
    ui->labelRangeSliderValue->setText(range.setNum(position));
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthSetAccelerationDoubleSpinBox_valueChanged(double azimuthAcceleration)
{
     MainWindow::sendCommand(1,3,azimuthAcceleration);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthSetPositionDoubleSpinBox_valueChanged(double azimuthPosition)
{
    MainWindow::sendCommand(1,2,azimuthPosition);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthSetSpeedDoubleSpinBox_valueChanged(double azimuthSpeed)
{
    MainWindow::sendCommand(1,0,azimuthSpeed);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthSetPeriodDoubleSpinBox_valueChanged(double azimuthPeriod)
{
    MainWindow::sendCommand(1,7,azimuthPeriod);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonSetAccelerationDoubleSpinBox_valueChanged(double horizonAcceleration)
{
    MainWindow::sendCommand(2,3,horizonAcceleration);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonSetPositionDoubleSpinBox_valueChanged(double  horizonPosition)
{
    MainWindow::sendCommand(2,2,horizonPosition);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonSetSpeedDoubleSpinBox_valueChanged(double horizonSpeed)
{
    MainWindow::sendCommand(2,0,horizonSpeed);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonSetPeriodDoubleSpinBox_valueChanged(double horizonPeriod)
{
    MainWindow::sendCommand(2,7,horizonPeriod);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_skyTrackButton_clicked()
{
    MainWindow::sendCommand(0,8,0);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_azimuthSetSpeedRatioDoubleSpinBox_valueChanged(double azimuthSpeedRatio)
{
    MainWindow::sendCommand(1,9,azimuthSpeedRatio);
}

//--------------------------------------------------------------------------------------------
void MainWindow::on_horizonSetSpeedRatioDoubleSpinBox_valueChanged(double horizonSpeedRatio)
{
     MainWindow::sendCommand(2,9,horizonSpeedRatio);
}
