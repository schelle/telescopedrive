/*!
 * \file telescopeDriveR2.ino
 * \date 2016/06/16
 *
 * \author schelle.igor@gmail.com
 *
 * \brief
 *
 * State machine
 *
 * \note
*/

//DRIVERS
// 1 azimut
// 2 horizon

//COMMANDS
// 0 set speed
// 1 stop
// 2 position                 34  "xx,x
// 3 acceleration
// 4 reference 0
// 5 gonioMode ON set phase   37  %xx,x
// 6 goniomode OFF
// 7 period                   39  'xx,x
// 8 automatic mode           40  (xx,x
// 9 spedRatio                41  )xx,x


//DIRECTION
// 0 CCV e.g.(16...-16)
// 1 CV  e.g.(-16...16)

#include "telescopeDrive.h"
  const int manualReadPin = 7;
  //const uint16_t testingMultiplicator = 1;// speed up the movement - good for faster testing
  String buff;
  float floatValue = 0;
  unsigned char incomingByte;
  int driveNumber = 0;
  int command;
  int timer = 1;

  int s3 = 10;
  int s4 = 13;

  Motor azimuthDrive(8, 9, A0);
  Motor horizonDrive(12, 11, A1);
 
void setup() { 
  pinMode(red_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  pinMode(blue_led, OUTPUT);
  pinMode(s3, OUTPUT); // microstep S3
  pinMode(s4, OUTPUT); // microstep S4
  pinMode(manualReadPin, INPUT);
  buff = "XXXX,";
  azimuthDrive.setMaxSpeed(10000);
  horizonDrive.setMaxSpeed(10000);
  Serial.begin(9600);  
}


void microstep(int microsteps)
{
  switch(microsteps){
    case 1:
      digitalWrite(s3, LOW);
      digitalWrite(s4, LOW);
      break;
    case 2:
      digitalWrite(s3, HIGH);
      digitalWrite(s4, LOW);
      break;
    case 8:
      digitalWrite(s3, HIGH);
      digitalWrite(s4, HIGH);
      break;
    case 16:
      digitalWrite(s3, LOW);
      digitalWrite(s4, HIGH);
      break;
  } 
}

void loop() {
//comunication
if (Serial.available()) {
  incomingByte = Serial.read();
  floatValue = Serial.parseFloat();

  driveNumber = (incomingByte & (7<<5)) >> 5;
  command = (incomingByte & 31);

  switch(command){
    case 0: // Set speed
      switch(driveNumber){
        case 1:
          azimuthDrive.setSpeed(floatValue);
          azimuthDrive.setCurrentState(SPEED);
          break;
        case 2: 
          horizonDrive.setSpeed(floatValue);
          horizonDrive.setCurrentState(SPEED);
          break;    
        } 
        break;
    case 1: //Stop
     switch(driveNumber){
      case 1: 
        azimuthDrive.stop();
        azimuthDrive.setCurrentState(STOP);
        break;    
      case 2:
        horizonDrive.stop();
        horizonDrive.setCurrentState(STOP);
        break;
      }   
      break; 
    case 2: //Set position
     switch(driveNumber){
      case 1: 
        azimuthDrive.moveTo(long(floatValue));
        azimuthDrive.setCurrentState(POS);
        break;    
      case 2:
        horizonDrive.moveTo(long(floatValue));
        horizonDrive.setCurrentState(POS);
        break;
      }   
      break;    
    case 3: //Set acceleration
     switch(driveNumber){
      case 1: 
        azimuthDrive.setAcceleration(floatValue);
        azimuthDrive.setCurrentState(POS);
        break;    
      case 2:
        horizonDrive.setAcceleration(floatValue);
        horizonDrive.setCurrentState(POS);
        break;
      }
      break;   
    case 4: //Reference 0
     switch(driveNumber){
      case 1: 
        azimuthDrive.setCurrentPosition(0);
        azimuthDrive.setCurrentState(POS);
        break;    
      case 2:
        horizonDrive.setCurrentPosition(0);
        horizonDrive.setCurrentState(POS);
        break;
      }
      break; 
     case 5:{ //Set Gonio Mode ON - set phase
          switch(driveNumber){
            case 1:{
              long gonioTargetPosition = -azimuthDrive.currentPosition();
              azimuthDrive.setGonioMode(true);
              azimuthDrive.setPhase((long)floatValue);
              azimuthDrive.moveTo(gonioTargetPosition);
              azimuthDrive.setCurrentState(GONIO);
              break;    
            }
            case 2:{
              long gonioTargetPosition = -horizonDrive.currentPosition();
              horizonDrive.setGonioMode(true);
              horizonDrive.setPhase((long)floatValue);
              horizonDrive.moveTo(gonioTargetPosition);
              horizonDrive.setCurrentState(GONIO);
              break;
            }
          }
        }
        break;  
     case 6: //Set Gonio Mode OFF
        horizonDrive.debug(3);
        switch(driveNumber){
        case 1: 
          azimuthDrive.setGonioMode(false);
          azimuthDrive.setCurrentState(POS);
          break;    
        case 2:
          horizonDrive.setGonioMode(false);
          horizonDrive.setCurrentState(POS);
          break;
        }
        break;
     case 7: //Set period
        horizonDrive.debug(2);
        switch(driveNumber){
        case 1: 
          azimuthDrive.setPeriod(floatValue);
          azimuthDrive.setCurrentState(POS);
          break;    
        case 2:
          horizonDrive.setPeriod(floatValue);
          horizonDrive.setCurrentState(POS);
          break;
        }
        break;
      case 8: { //Auto Sky Tracking mode
          double azimuthCurrentPosition = (double)azimuthDrive.currentPosition();
          double horizonCurrentPosition = (double)horizonDrive.currentPosition();
          double diameter;
          
          if ( (azimuthCurrentPosition == 0) and (horizonCurrentPosition != 0) ){
            diameter = horizonCurrentPosition;
          }
          else if ( (horizonCurrentPosition == 0) and (azimuthCurrentPosition != 0)  ){
            diameter = azimuthCurrentPosition;
          }
          else if ( (horizonCurrentPosition == 0) and (azimuthCurrentPosition == 0)  ){
            return;
          }
          else if ( (horizonCurrentPosition != 0) and (azimuthCurrentPosition != 0)  ){
           diameter = abs(azimuthCurrentPosition/(sin(atan(azimuthCurrentPosition/horizonCurrentPosition))));
          }
          
          //azimuthDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/4)/diameter)/24  );
          //horizonDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/4)/diameter)/24  );

          //azimuthDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/4)/diameter)  );
          //horizonDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/4)/diameter)  );
          
          //azimuthDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/4)/diameter)/(1) );
          //horizonDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/4)/diameter)/(0.649448  ));

          //azimuthDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/2)/diameter)/(1) );
          //horizonDrive.setPeriod(   ((SECONDS_PER_REVOLUTION/2)/diameter)/(0.649448  ));

          azimuthDrive.setPeriod(   SECONDS_PER_REVOLUTION/(2*diameter*azimuthDrive.speedRatio() )   );
          horizonDrive.setPeriod(   SECONDS_PER_REVOLUTION/(2*diameter*0.649448*horizonDrive.speedRatio() )   );
    
          azimuthDrive.setGonioMode(true);
          horizonDrive.setGonioMode(true);
          azimuthDrive.setPhase(azimuthDrive.currentPosition());
          horizonDrive.setPhase(horizonDrive.currentPosition());

          long azimuthDiameter = 0;
          long horizonDiameter = 0;

          if(azimuthCurrentPosition >= 0 && horizonCurrentPosition >= 0){
            azimuthDiameter = -diameter;
            horizonDiameter = diameter;
          }
          else if(azimuthCurrentPosition < 0 && horizonCurrentPosition >= 0){      
            azimuthDiameter = -diameter; 
            horizonDiameter = -diameter;      
          }
          else if(azimuthCurrentPosition < 0 && horizonCurrentPosition < 0){
            azimuthDiameter = diameter;
            horizonDiameter = -diameter;
           
          }
          else if(azimuthCurrentPosition >= 0 && horizonCurrentPosition < 0){
            azimuthDiameter = diameter;
            horizonDiameter = diameter;
          }

//          if(azimuthCurrentPosition >= 0 && horizonCurrentPosition >= 0){
//            azimuthDiameter = diameter;
//            horizonDiameter = -diameter;
//          }
//          else if(azimuthCurrentPosition < 0 && horizonCurrentPosition >= 0){      
//            azimuthDiameter = diameter; 
//            horizonDiameter = diameter;      
//          }
//          else if(azimuthCurrentPosition < 0 && horizonCurrentPosition < 0){
//            azimuthDiameter = -diameter;
//            horizonDiameter = diameter;
//           
//          }
//          else if(azimuthCurrentPosition >= 0 && horizonCurrentPosition < 0){
//            azimuthDiameter = -diameter;
//            horizonDiameter = -diameter;
//          }
   
   
          azimuthDrive.moveTo(azimuthDiameter);
          horizonDrive.moveTo(horizonDiameter);
          
          azimuthDrive.setCurrentState(GONIO);
          horizonDrive.setCurrentState(GONIO);
          
          Serial.print("azimuthCurrentPosition: ");
          Serial.print(azimuthCurrentPosition );
          Serial.print(" horizonCurrentPosition: ");
          Serial.print(horizonCurrentPosition);
          Serial.print(" azimuthDiameter: ");
          Serial.print(azimuthDiameter); 
          Serial.print(" horizonDiameter: ");
          Serial.print(horizonDiameter);
          Serial.print(" azimuthTarget: ");
          Serial.print(azimuthDrive.targetPosition()); 
          Serial.print(" horizonTarget: ");
          Serial.print(horizonDrive.targetPosition());
          Serial.print(" azimuthDirection: ");
          Serial.print(azimuthDrive.moveDirection()); 
          Serial.print(" horizonDirection: ");
          Serial.println(horizonDrive.moveDirection()); 
          }
        break;  
      case 9: //Set speedRatio
        switch(driveNumber){
        case 1: 
          azimuthDrive.setSpeedRatio(floatValue);
         //-----------------------
         Serial.print(" Speed ratio a: ");
         Serial.println(azimuthDrive.speedRatio()); 
         Serial.print(" Speed ratio h: ");
         Serial.println(horizonDrive.speedRatio()); 
         //-----------------------------------------
          break;    
        case 2:
          horizonDrive.setSpeedRatio(floatValue);
          break;
        }
        break;   
  } 

} 

if(digitalRead(manualReadPin) == LOW){
  if(   (horizonDrive.getCurrentState() == MAN) && (azimuthDrive.getCurrentState() == MAN)   ){     
    horizonDrive.setUpPreviousState();
    azimuthDrive.setUpPreviousState();
  }
  else{   
    horizonDrive.setCurrentState(MAN);
    azimuthDrive.setCurrentState(MAN);
  }
  delay(200);
}
      if(!(timer%10000)){
        buff = "";
        buff += horizonDrive.currentPosition();
        Serial.println(buff);
        timer = 1;
        timer++;
      }
      else if(!(timer%5000)){
        buff = "";
        buff += azimuthDrive.currentPosition();
        buff += ",";
        Serial.print(buff);
        timer++;
      }
      else{
        azimuthDrive.smartRun();
        horizonDrive.smartRun();
        delay(0.1);
        timer++;
      } 
}
