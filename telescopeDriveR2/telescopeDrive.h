/*!
 * \file telescopeDrive.h
 * \date 2016/06/16
 *
 * \author schelle.igor@gmail.com
 *
 * \brief
 *
 * Derived class for controlling stepper motor
 * wia any driver with extra for telescope star tracking
 *
 * \note
*/

#ifndef telescopeDrive
#define telescopeDrive
#include <AccelStepper.h>
#include <math.h>

#define _USE_MATH_DEFINES
#define SECONDS_PER_REVOLUTION 86400 // [s]
#define red_led 2
#define green_led 3
#define blue_led 4

/// Stepper states
enum state {STOP, SPEED, POS, MAN, GONIO};
/// Drive types 
enum drive {AZIMUTH, HORIZON};

class Motor: public AccelStepper {
  public:
    Motor(int clk, int dir, int manInput);
    
	  Motor(Motor const &) = delete;
    
	  Motor &operator = (Motor const &) = delete;
   
	  Motor() {}
    
    state getCurrentState() {return currentState;}
    
    state getPreviousState() {return previousState;}
    
    void setCurrentState(state cState);
    
    void setUpPreviousState() {currentState = previousState;}
    
    /// Performs movement acording to current motor state 
    void smartRun(void);

    /// Debug method LED blinking
    void debug(int flashes);

    /// Debug method for color LED blinking
    void debugClr(int flashes, double period, char color);

    /// Stars tracking 
    void runGonio();

    void setGonioMode(bool goniomode);

    /// Sets initial phase
    void setPhase(long phase);

    /// Sets time per revolution
    void setPeriod(double period);
    
    double period();

    /// returns direction 0 CCV , 1 CV 
    int moveDirection();

    void setSpeedRatio(double ratio);

    // fine adjistment of speed in gonio mode
    double speedRatio(){return _speedRatio;}
    
  private:
    /// Calculates step duration depending on stepper state - including star trackink mode 
    void computeNewSpeed();

    /// Function calculating step duration for star tracking mode 
    void setTimeIntervalForStarTracking();
    
    inline bool isInitialCalculationRequired() {return initialCalculation;}
    
    inline bool isGonioModeOn() {return _gonioMode;}
    
    state           currentState;
    state           previousState;
    bool            _gonioMode = false;
    bool            initialCalculation = false;
    int             joysticValue = 0;
    int             joysticSpeed = 0;
    int             manualPin;
    int             counter = 0;
    long            _amplitude = 0.0;
    unsigned long   _phase = 0;
    float           _elevation = 0.0;
    float           _previousElevation  = 0.0;
    double          _speedRatio = 1;
    double          _period = 1;
    
};

#endif //__telescopeDrive
