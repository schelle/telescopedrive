#include "telescopeDrive.h"
#include <math.h>

//-------------------------------------------------------------------------------------------------------------------------
void Motor::setCurrentState(state cState)
{
  previousState = currentState;
  currentState = cState;
}

//-------------------------------------------------------------------------------------------------------------------------
Motor::Motor(int clk, int dir, int manInput) : AccelStepper(AccelStepper::DRIVER, clk, dir)
{
  currentState = SPEED;
  previousState = SPEED;
  manualPin = manInput;
} 

//-------------------------------------------------------------------------------------------------------------------------
void Motor::smartRun(void)
{
  switch(this->currentState){
  case SPEED:  
    this->runSpeed(); break; 
  case POS:
    this->run(); break;
  case GONIO:
    this->runGonio(); break;
  case STOP:
    break;
  case MAN:

    joysticValue = analogRead(manualPin);

    if( ((joysticValue  > 530) || (joysticValue < 490)) ){
      joysticSpeed = map(joysticValue, 0, 1023, -4000, 4000);     
//      joysticSpeed = map(joysticValue, 0, 1023, -10000, 10000);
//      if( (joysticSpeed >= -1000) && (joysticSpeed <= 1000) ){
//        joysticSpeed *= 0.01;
//      }
//      else if( (joysticSpeed < -7000) || (joysticSpeed > 7000) ){
//        joysticSpeed *= 2;
//      }
      this->setSpeed(joysticSpeed);
      this->runSpeed();
    break;
    } 
  }
}

//-------------------------------------------------------------------------------------------------------------------------
void Motor::debugClr(int flashes, double period, char color)
{
  int ledOut = 0;
  switch(color){
    case 'r':
      ledOut = 2;
      break;
    case 'g':
       ledOut = 3;
       break;
    case 'b':
      ledOut = 4;
      break;    
    }
  for(int i = 0; i < flashes; i++){
    digitalWrite(ledOut, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(period);              // wait for a second
    digitalWrite(ledOut, LOW);    // turn the LED off by making the voltage LOW
    delay(period);
    }
  }
  
//-------------------------------------------------------------------------------------------------------------------------
void Motor::debug(int flashes)
{
  for(int i = 0; i < flashes; i++){
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(200);              // wait for a second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    delay(200);
    }
}

//-------------------------------------------------------------------------------------------------------------------------
void Motor::runGonio()
{
  if(isInitialCalculationRequired()){
    computeNewSpeed();
  }
  
  if (runSpeed()){
    computeNewSpeed();
  }

  if(_gonioMode == true){
    if(   _speed == 0.0 || distanceToGo() == 0  ){
      moveTo(-targetPosition());  
      initialCalculation = true;
    }
  }
}

//-------------------------------------------------------------------------------------------------------------------------
void Motor::setPhase(long phase)
{
     initialCalculation = true;
      _currentPos = phase;
}

//-------------------------------------------------------------------------------------------------------------------------
double Motor::period()
{
  return _period; 
}

//-------------------------------------------------------------------------------------------------------------------------
void Motor::setPeriod(double period)
{
  _period = period;
}

//-------------------------------------------------------------------------------------------------------------------------
int Motor::moveDirection()
{
  return _direction;
}
//-------------------------------------------------------------------------------------------------------------------------
void Motor::setGonioMode(bool goniomode)
{
   _gonioMode = goniomode;
   debug(3);
}

//-------------------------------------------------------------------------------------------------------------------------
void Motor::setSpeedRatio(double ratio)
{ 
  _speedRatio = ratio;
  debug(1);
}

//-------------------------------------------------------------------------------------------------------------------------
void Motor::setTimeIntervalForStarTracking()
{
  
  //_stepInterval = (1000000.0*period())  * (   abs(  _amplitude*cos( (M_PI/(2*_amplitude))*(counter + 1) ) - _amplitude*cos( (M_PI/(2*_amplitude))*(counter) )  )   ); // [us]
   
  _stepInterval = (1000000.0*period())  * (   abs(  _amplitude*cos( (M_PI/(2*_amplitude))*(counter + 1) ) - _amplitude*cos( (M_PI/(2*_amplitude))*(counter) )  )   ); // [us]

  if( _stepInterval == 0){
    _stepInterval = 1;
    }
  }

//-------------------------------------------------------------------------------------------------------------------------
void Motor::computeNewSpeed()
{
    long distanceTo = distanceToGo(); // +ve is clockwise from curent location

    long stepsToStop = (long)((_speed * _speed) / (2.0 * _acceleration));

    if (distanceTo == 0 && stepsToStop <= 1 && initialCalculation == false){
      // We are at the target and its time to stop
      _stepInterval = 0;
      _speed = 0.0;
      _n = 0;
      return;
    }

    if (distanceTo > 0){
      // We are anticlockwise from the target
      // Need to go clockwise from here, maybe decelerate now
      if (_n > 0){
          // Currently accelerating, need to decel now? Or maybe going the wrong way?
          if ((stepsToStop >= distanceTo) || _direction == DIRECTION_CCW)
            _n = -stepsToStop; // Start deceleration
      }
      else if (_n < 0){
          // Currently decelerating, need to accel again?
          if ((stepsToStop < distanceTo) && _direction == DIRECTION_CW)
            _n = -_n; // Start accceleration
      }
    }
    else if (distanceTo < 0){
      // We are clockwise from the target
      // Need to go anticlockwise from here, maybe decelerate
      if (_n > 0){
          // Currently accelerating, need to decel now? Or maybe going the wrong way?
          if ((stepsToStop >= -distanceTo) || _direction == DIRECTION_CW)
        _n = -stepsToStop; // Start deceleration
      }
      else if (_n < 0){
          // Currently decelerating, need to accel again?
          if ((stepsToStop < -distanceTo) && _direction == DIRECTION_CCW)
        _n = -_n; // Start accceleration
      }
    }

    // Need to accelerate or decelerate
    if (_n == 0){
      debug(2);
      //! First step from stopped
      _cn = _c0;
      _direction = (distanceTo > 0) ? DIRECTION_CW : DIRECTION_CCW;
      Serial.print("Dir: ");
      Serial.println(_direction);
    }
    else{
      // Subsequent step. Works for accel (n is +_ve) and decel (n is -ve).
      _cn = _cn - ((2.0 * _cn) / ((4.0 * _n) + 1)); // Equation 13
      _cn = max(_cn, _cmin); 
    }

    //precalculation - calculates first step only
    if(isInitialCalculationRequired() && isGonioModeOn()){
      initialCalculation = false;
      counter = currentPosition();
     _amplitude = abs(_targetPos);
     setTimeIntervalForStarTracking();
    }
    else{  // Main gonio loop
      if(isGonioModeOn()){ 
        _n++;
        counter = _direction ?   (counter + 1) : (counter - 1)  ;
        setTimeIntervalForStarTracking();           
        if (_direction == DIRECTION_CCW)
          _speed = -_speed;
      }
      else{ // regular movement
          _n++;
          _stepInterval = _cn;
          _speed = 1000000.0 / _cn;
          if (_direction == DIRECTION_CCW)
            _speed = -_speed;   
      }
    }
   #if 0
    Serial.print(" period ");
    Serial.print(period());
    Serial.print(" gonioMode ");
    Serial.print(isGonioModeOn());
    Serial.print(" initialCalculation ");
    Serial.print(initialCalculation);
    Serial.print(" counter: ");
    Serial.print( counter);
    Serial.print(" curPos: ");
    Serial.print(currentPosition());
    Serial.print(" tarPos ");
    Serial.print(_targetPos);
    Serial.print(" dst2go ");
    Serial.print(distanceTo);
    Serial.print(" sIint ");
    Serial.print(_stepInterval);
    Serial.print(" direction ");
    Serial.print(_direction);
    Serial.print(" _amplitude ");
    Serial.println(_amplitude);
   #endif

}
